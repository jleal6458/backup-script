# -*- coding: utf-8 -*-

import os
import zipfile
import smtplib
import time
import sys
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email import encoders


# email from and email to
# note that the sender and receiver CANT be the same, and has to be outlook. Receiver can be
# gmail but the email won't be received, the data will be stored in the sender's sended messages
email_user = "sender@outlook.es"
email_password = "pwd"
email_to = "receiver@outlook.es"


# Use a temp dir to create and delete the zip dir. If we create the zip in the same dir as the one
# we want to backup we basically create a virus
zip_dir = "C:/tmp"
zip_filename = os.path.join(zip_dir, "backup.zip")

# create zip in specified path
with zipfile.ZipFile(zip_filename, 'w', zipfile.ZIP_DEFLATED) as zipf:
    for file in os.listdir():
        if os.path.isfile(file):
            zipf.write(file)

# outlook wont let you send a zip file bigger than 20MB and an email with more than 20MB in total size
# no matter the number of files
def split_zip(file_path, max_size):
    part_num = 1
    total_size = os.path.getsize(file_path)
    with open(file_path, 'rb') as original_zip:
        while True:
            data = original_zip.read(max_size)
            if not data:
                break
            part_filename = os.path.join(zip_dir, f"backup_part{part_num}.zip")
            with open(part_filename, 'wb') as part_file:
                part_file.write(data)
            yield part_filename
            part_num += 1

# do the split thing:
max_zip_size = 19 * 1024 * 1024  # 19 MB
attachments = []
for part_filename in split_zip(zip_filename, max_zip_size):
    attachment = open(part_filename, 'rb')
    
    msg = MIMEMultipart()
    msg['From'] = email_user
    msg['To'] = email_to
    subject_arg = sys.argv[1] if len(sys.argv) > 1 else ""  # we pass something as argument and that will be the subject of the email
    msg['Subject'] = subject_arg if subject_arg else "Backup de archivos"  # default subject

    part = MIMEBase('application', 'octet-stream')
    part.set_payload(attachment.read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', f"attachment; filename= {os.path.basename(part_filename)}")
    msg.attach(part)
    
    # use office smtp server
    server = smtplib.SMTP('smtp.office365.com', 587)
    server.starttls()
    server.login(email_user, email_password)
    text = msg.as_string()
    server.sendmail(email_user, email_to, text)
    server.quit()
    
    attachments.append(part_filename)

try:
    time.sleep(1)
    os.remove(zip_filename)
    for part_filename in attachments:
        os.remove(part_filename)
except PermissionError as e:
    print("Error deleting files:", e)

print("Backup succesfully sendt to", email_to)
